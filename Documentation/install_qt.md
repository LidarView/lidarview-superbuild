# Install Qt5

Qt5 is built automatically by the Superbuild, however to speed up the build process, you can opt to use built-binaries with the following options:

 - Using Qt5.15.2 [offline installer](https://www.qt.io/offline-installers):

    * Note that only the `Desktop 64-bit` component is needed
    * Installation location:
      - On Unix we recommend a `/opt` installation, and adding this directory to your ld configuration using:
        `sudo echo "/usr/local/lib" >> /etc/ld.so.conf && sudo ldconfig`
      - On Windows we recommend a `C:\` installation

 - Using Qt5.15.7 [installer](https://www.qt.io/download-open-source#contributing)

 - If you system's package manager offers Qt5 with version 5.15.2 or higher (e.g Ubuntu20.04) use:
    `qt5-default qtmultimedia5-dev qtbase5-private-dev libqt5x11extras5-dev libqt5svg5-dev libqt5xmlpatterns5 qttools5-dev qtxmlpatterns5-dev-tools`

- Additionally For Ubuntu22:
    `qt5-default` is now only available for 20.04 and earlier. Use:
    `sudo apt-get install qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools qtmultimedia5-dev qtbase5-private-dev libqt5x11extras5-dev libqt5svg5-dev qttools5-dev qtxmlpatterns5-dev-tools libqt5help5`

## Test Qt installation

You can test that `Qt5` has been correctly installed with `qmake -v`, which should give you the `Qt` version installed. 

## Configure with installed Qt5 <a name="configure-with-qt"></a>

Add proper parameters to CMake superbuild configuration options:
```
cmake ../<path-to-superbuild-src> -GNinja -DUSE_SYSTEM_qt5=ON -DQt5_DIR="/path/to/install/location/lib/cmake/Qt5"
```
Depending on your Qt installation `-DQt5_DIR` may not be necessary.

**Always forward slashes, UNIX style, on all platforms**
- e.g If installed in `/opt`: `-DQt5_DIR=/opt/Qt5.15.2/5.15.2/gcc_64/lib/cmake/Qt5`
- e.g If installed in `C:\` : `-DQt5_DIR=C:/Qt/5.15.2/msvc2019_64/lib/cmake/Qt5`

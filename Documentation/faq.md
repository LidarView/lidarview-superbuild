# Troubleshooting / FAQ <a name="faq-instructions"></a>

## **UBUNTU** Cannot find Qt Packages "unable to locate package qt5-default"

Qt is [community software](https://packages.ubuntu.com/focal/qt5-default), Uncomment / add the `universe` ubuntu PPA in your `/etc/apt/sources.list`

Example: `deb http://archive.ubuntu.com/ubuntu/ focal universe`

## Superbuild failure with PCL enabled

Depending on your hardware, when enabling (`-DENABLE_pcl=True`) the superbuild might fail during PCL compilation with an *Internal compiler error* due to intense memory allocation.

To work around this issue you can try:
 - Re-running the build command, as successive incremental builds may eventually succeed.
 - Lowering the number of compilation jobs in the build command using the `-j N` option.

## How to debug LidarView with Visual Studio on Windows <a name="debug-on-visual-studio"></a>

- First compile `LidarView` with `-DCMAKE_BUILD_TYPE_lidarview=RelWithDebInfo`. (You might also want to set it for `ParaView`: `-DCMAKE_BUILD_TYPE_paraview=RelWithDebInfo`).
- Open corresponding `Visual Studio` app, `Debug > Attach to Process...` and then search for `LidarView` process.  (It might need to set a pause at the beggining of LidarView code, if a crash happen at the start)
- You can now set break point and have a trace!

## How to use QtCreator with LidarView and the superbuild

To use LidarView in QtCreator you will need:
- a successful lidarview superbuild
- add in `Tools > Options > Kits > CMake` the `CMakeCache.txt` located at `<superbuild-build-folder>/superbuild/lidarview/build/`.

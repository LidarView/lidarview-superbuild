include(paraview-version)

set(paraview_plugin_path "lib/paraview-${paraview_version}/plugins")
set(lidarview_plugin_path "lib/lidarview/plugins")
set(slam_plugin_path "lib/slam/plugins")
set(ros2io_plugin_path "lib/ros2io/plugins")
set(pclplugin_plugin_path "lib/pclplugin/plugins")

# Trigger lidarview common bundling
include(lidarview.bundle.common)

# Trigger common lidarview based app bundling
include(lidarviewapp.bundle)

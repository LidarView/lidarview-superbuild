include(lidarview-version)
include(paraview-version)

set(SOFTWARE_NAME "LidarView")

# Enable CPack packaging.
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
  "LidarView performs real-time reception, recording, visualization and processing of 3D LiDAR data.")
if (NOT DEFINED CPACK_PACKAGE_NAME)
  set(CPACK_PACKAGE_NAME "${SOFTWARE_NAME}")
endif ()
set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_PACKAGE_VERSION_MAJOR "${lidarview_version_major}")
set(CPACK_PACKAGE_VERSION_MINOR "${lidarview_version_minor}")
set(CPACK_PACKAGE_VERSION_PATCH "${lidarview_version_patch}")

set(SOFTWARE_ICON_PATH "${CMAKE_CURRENT_LIST_DIR}/files/lidarview.ico")

# WiX does not support non-dotted version numbers. See below.
if (NOT cpack_generator STREQUAL "WIX")
  string(APPEND CPACK_PACKAGE_VERSION_PATCH "${lidarview_version_suffix}")
endif ()
set(name_suffix "")
if (lidarview_version_branch)
  set(name_suffix "-${lidarview_version_branch}")
  # A lot of Lidarview branches are using / in their name
  string(REPLACE "/" "" name_suffix ${name_suffix})
endif ()

set(CPACK_PACKAGE_FILE_NAME
  "${CPACK_PACKAGE_NAME}${name_suffix}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
# Append the version suffix here though. See above.
if (cpack_generator STREQUAL "WIX")
  string(APPEND CPACK_PACKAGE_FILE_NAME "${lidarview_version_suffix}")
endif ()

set(lidarview_extra_config_files "${superbuild_install_location}/share/interface_modes_config.json")

# Trigger common lidarview based app bundling
include(lidarviewapp.bundle.common)

list(APPEND lidarview_executables
  PacketFileSender)

list(APPEND python_modules
  VelodynePlugin
  lidarviewpythonplugin

  camera_path
  colormap_tools
  temporal_animation_cue_helpers
  example_temporal_animation
  example_non_temporal_animation
)

# Set LidarView license file.
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_LIST_DIR}/files/lidarview.license.txt")


file(GLOB include_files "${source_dir}/cpp/mcap/include/mcap/*.hpp")
file(
  INSTALL ${include_files}
  DESTINATION "${install_dir}/include/mcap")

file(GLOB impl_include_files "${source_dir}/cpp/mcap/include/mcap/*.inl")
file(
  INSTALL ${impl_include_files}
  DESTINATION "${install_dir}/include/mcap")

set(tins_platform_dependencies)
if (WIN32)
  list(APPEND tins_platform_dependencies
    npcap)
endif ()

superbuild_add_project(tins
  DEPENDS pcap boost
  DEPENDS_OPTIONAL ${tins_platform_dependencies}
  LICENSE_FILES
    LICENSE

  CMAKE_ARGS
    -DCMAKE_INSTALL_RPATH:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_LIBDIR:PATH=lib
    -DLIBTINS_BUILD_EXAMPLES:BOOL=OFF
    -DLIBTINS_BUILD_TESTS:BOOL=OFF
)

superbuild_apply_patch(tins fix-cmake-config-install "Fix cmake config install")

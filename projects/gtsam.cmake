superbuild_add_project(gtsam
  DEPENDS cxx17 eigen boost
  LICENSE_FILES
    LICENSE.BSD

  CMAKE_ARGS
  -DCMAKE_BUILD_TYPE:STRING=Release
  -DGTSAM_USE_SYSTEM_EIGEN:BOOL=ON
  -DGTSAM_BUILD_TESTS:BOOL=OFF
  -DGTSAM_BUILD_EXAMPLES_ALWAYS:BOOL=OFF
  -DGTSAM_UNSTABLE_BUILD_PYTHON:BOOL=OFF
  -DGTSAM_BUILD_UNSTABLE:BOOL=OFF
  -DGTSAM_INSTALL_MATLAB_TOOLBOX:BOOL=OFF
  -DGTSAM_WITH_TBB:BOOL=OFF
  -DGTSAM_DISABLE_NEW_TIMERS:BOOL=OFF
  -DGTSAM_INSTALL_CPPUNITLITE:BOOL=OFF
  -DCMAKE_INSTALL_RPATH:BOOL=<INSTALL_DIR>/lib
  -DCMAKE_INSTALL_LIBDIR:STRING=lib
  -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
  # Pb of metis dep at packaging
  # see https://github.com/borglab/gtsam/issues/380 for more details
  # Next param disables metis use
  # Warning : graph optimization might be slower than with metis
  -DGTSAM_SUPPORT_NESTED_DISSECTION:BOOL=OFF
)

superbuild_apply_patch(gtsam fix-boost-not-found "Fix boost not found")
superbuild_apply_patch(gtsam fix-infinite-build-loop "Fix infinite build loop")

superbuild_add_project(clipper
  DEPENDS scs lapack cxx17 eigen boost pcl
  LICENSE_FILES
    LICENSE

  CMAKE_ARGS
    -DCMAKE_INSTALL_RPATH:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_LIBDIR:PATH=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}

    -DCLIPPER_BUILD_BINDINGS_PYTHON:BOOL=OFF
    -DCLIPPER_BUILD_BINDINGS_MATLAB:BOOL=OFF
    -DCLIPPER_BUILD_TESTS:BOOL=OFF
    -DCLIPPER_ENABLE_MKL:BOOL=OFF
    -DCLIPPER_ENABLE_BLAS:BOOL=ON
    -DCLIPPER_ENABLE_SCS_SDR:BOOL=ON    
)

superbuild_apply_patch(clipper include-blas-lapack "This add 'find_package(lapack)' to the project, else the linker will fail to find -lbas and -lapack")
superbuild_apply_patch(clipper remove-compile-definitions "This patch removes compile definitions to make the library compatible with teaser")

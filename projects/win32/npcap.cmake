if(CMAKE_CL_64)
  set(wpcap_library_dir <BINARY_DIR>/Lib/x64)
else()
  set(wpcap_library_dir <BINARY_DIR>/Lib)
endif()

# Override winpcap library with npcap one
# User MUST install npcap dll on his side
superbuild_add_project(npcap
  DEPENDS pcap
  BUILD_IN_SOURCE 1
  LICENSE_FILES
    "${CMAKE_CURRENT_LIST_DIR}/../files/libpcap.LICENSE.txt"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ""
  INSTALL_COMMAND ${CMAKE_COMMAND}
    -DPCAP_STATIC_LIBRARY:FILEPATH=${wpcap_library_dir}/wpcap.lib
    -DPCAP_INSTALL_DIR:PATH=<INSTALL_DIR>
	  -DPCAP_INCLUDE_DIR:PATH=<BINARY_DIR>/Include
    -P ${CMAKE_CURRENT_LIST_DIR}/pcap.install.cmake
)

superbuild_project_add_step(npcap-remove-shared-libs
  COMMAND "${CMAKE_COMMAND}" "-E" "remove" "-f" "<INSTALL_DIR>/bin/wpcap.dll" "<INSTALL_DIR>/bin/Packet.dll"
  DEPENDEES install
  COMMENT "Remove existing winpcap dll"
  WORKING_DIRECTORY <SOURCE_DIR>)

superbuild_add_extra_cmake_args(
  -DPCAP_LIBRARY:FILEPATH=<INSTALL_DIR>/lib/wpcap.lib)

include(lidarview-version)
include(paraview-version)

set(lidarview_executables
  "${SOFTWARE_NAME}"
  lvserver)
if (python3_enabled)
  list(APPEND lidarview_executables
    lvbatch
    lvpython)
endif ()

# lidarview is only build shared, see paraview.bundle.common if we need to support static build
set(python_modules
  paraview
  vtk
  vtkmodules
  lidarview
  lidarviewcore
)

macro (check_for_python_module project module)
  if (${project}_built_by_superbuild)
    list(APPEND python_modules
      "${module}")
  endif ()
endmacro ()

check_for_python_module(numpy numpy)

# Get LV & PV Plugins
function (lidarview_detect_plugins plugins_dir output)
  set(plugins_list)
  file(GLOB subpaths LIST_DIRECTORIES TRUE RELATIVE ${plugins_dir} ${plugins_dir}/*)
  foreach (subpath ${subpaths})
    if (IS_DIRECTORY ${plugins_dir}/${subpath})
      list(APPEND plugins_list ${subpath})
    endif ()
  endforeach ()
  set(${output} ${plugins_list} PARENT_SCOPE)
endfunction ()

# Get Lidarview Plugins Name from directories
set(lidarview_plugins)
lidarview_detect_plugins("${superbuild_install_location}/${lidarview_plugin_path}" lidarview_plugins)
if (NOT lidarview_plugins)
  message(FATAL_ERROR "lidarview_plugins is empty")
endif ()

list(APPEND lidarview_plugins ${lidarview_additional_plugins})

# Get Paraview Plugins Name from directories
set(paraview_plugins)
lidarview_detect_plugins("${superbuild_install_location}/${paraview_plugin_path}" paraview_plugins)

# Set the license files.
set(qt_license_file "${CMAKE_CURRENT_LIST_DIR}/../pvsb/projects/files/Qt5.LICENSE")

function (lidarview_install_calibration project extension)
  if (${project}_enabled)
    file(GLOB shared_files "${superbuild_install_location}/share/*.${extension}")
    install(
      FILES       ${shared_files}
      DESTINATION "${lidarview_share_folder_path}"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function(lidarview_install_extra_config_files)
  foreach (files IN LISTS lidarview_extra_config_files)
    if (EXISTS "${files}")
      install(
        FILES       "${files}"
        DESTINATION "${lidarview_share_folder_path}"
        COMPONENT   superbuild)
    else ()
      message(FATAL_ERROR "${files}")
    endif ()
  endforeach ()
endfunction()

function (lidarview_install_license project)
  if (EXISTS "${superbuild_install_location}/share/licenses/${project}")
    install(
      DIRECTORY   "${superbuild_install_location}/share/licenses/${project}"
      DESTINATION "${lidarview_license_path}"
      COMPONENT   superbuild)
  else ()
    message(FATAL_ERROR "${superbuild_install_location}/share/licenses/${project} does not exist, aborting.")
  endif ()
endfunction ()

function (lidarview_install_all_licenses)
  set(license_projects "${enabled_projects}")

  foreach (project IN LISTS license_projects)
    if (NOT ${project}_built_by_superbuild)
      list(REMOVE_ITEM license_projects ${project})
    endif ()
  endforeach ()

  # paraview install itself in ParaView directory
  if (paraview IN_LIST license_projects)
    list(REMOVE_ITEM license_projects paraview)
    list(APPEND license_projects ParaView)
  endif ()

  foreach (project IN LISTS license_projects)
    lidarview_install_license("${project}")
  endforeach ()

  # When packaging system qt, install the license manually
  if (qt5_plugin_paths)
    install(
      FILES       "${qt_license_file}"
      DESTINATION "${lidarview_license_path}/qt5"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function (lidarview_install_extra_data)
  lidarview_install_all_licenses()

  lidarview_install_calibration(velodynesdk "xml")
  lidarview_install_calibration(hesaisdk "csv")

  if (lidarview_extra_config_files)
    lidarview_install_extra_config_files()
  endif ()
endfunction ()

if (qt5_enabled)
  include(qt5.functions)

  set(qt5_plugin_prefix)
  if (NOT WIN32)
    set(qt5_plugin_prefix "lib")
  endif ()

  # Add SVG support, so ParaView can use SVG icons
  set(qt5_plugins
    iconengines/${qt5_plugin_prefix}qsvgicon
    imageformats/${qt5_plugin_prefix}qsvg
    imageformats/${qt5_plugin_prefix}qjpeg
    sqldrivers/${qt5_plugin_prefix}qsqlite)

  if (WIN32)
    list(APPEND qt5_plugins
      platforms/qwindows)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/qwindowsvistastyle)
    endif ()
  elseif (APPLE)
    list(APPEND qt5_plugins
      platforms/libqcocoa
      printsupport/libcocoaprintersupport)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/libqmacstyle)
    endif ()
  elseif (UNIX)
    list(APPEND qt5_plugins
      platforms/libqxcb
      platforminputcontexts/libcomposeplatforminputcontextplugin
      xcbglintegrations/libqxcb-glx-integration)
  endif ()

  superbuild_get_qt5_plugin_install_paths(qt5_plugin_paths ${qt5_plugins})
else ()
  set(qt5_plugin_paths)
endif ()

foreach (bundle_file IN LISTS lidarview_additional_bundle_files)
  include(${bundle_file})
endforeach ()

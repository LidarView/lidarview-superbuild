set(paraview_binary_dir)
if (TARGET paraview)
  set(paraview_binary_dir "<BINARY_DIR>")
  _ep_replace_location_tags(paraview paraview_binary_dir)
endif ()

superbuild_add_project(pclplugin
  DEPENDS paraview pcl boost
  DEPENDS_OPTIONAL qt5 python3
  LICENSE_FILES
    LICENSE

  CMAKE_ARGS
    -DBUILD_TESTING:BOOL=OFF
    -DCMAKE_INSTALL_LIBDIR:PATH=lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib

    -DPCL_PLUGIN_EXPERIMENTAL_SURFACE_FILTERS:BOOL=ON
)

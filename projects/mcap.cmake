superbuild_add_project(mcap
  DEPENDS_OPTIONAL zstd lz4
  LICENSE_FILES
    LICENSE
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ""
  INSTALL_COMMAND
    "${CMAKE_COMMAND}"
      -Dsource_dir:PATH=<SOURCE_DIR>
      -Dinstall_dir:PATH=<INSTALL_DIR>
      -P "${CMAKE_CURRENT_LIST_DIR}/scripts/mcap.install.cmake"
  INSTALL_DEPENDS "${CMAKE_CURRENT_LIST_DIR}/scripts/mcap.install.cmake"
)

superbuild_add_extra_cmake_args(
  -Dmcap_INCLUDE_DIR:PATH=<INSTALL_DIR>/include
)

superbuild_add_project(flann
  DEPENDS cxx17 lz4
  LICENSE_FILES
    COPYING
  CMAKE_ARGS
   -DBUILD_EXAMPLES:BOOL=OFF
   -DBUILD_TESTS:BOOL=OFF
   -DBUILD_DOC:BOOL=OFF
   -DBUILD_PYTHON_BINDINGS:BOOL=OFF
   -DBUILD_MATLAB_BINDINGS:BOOL=OFF
   -DCMAKE_INSTALL_LIBDIR:STRING=lib
   -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
)

superbuild_apply_patch(flann remove-pkgconfig-deps "Remove pkgconfig dependency")
superbuild_apply_patch(flann rng-deterministic-thread-safe "Make random number generation deterministic and thread safe.")

superbuild_add_project(scs
  DEPENDS lapack
  LICENSE_FILES
    LICENSE.txt

  CMAKE_ARGS
    -DCMAKE_INSTALL_RPATH:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_LIBDIR:PATH=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
)

superbuild_apply_patch(scs include-blas-lapack "This add 'find_package(lapack)' to the project, else the linker will fail to find -lbas and -lapack")

if (paraview_enabled)
  set(vtk_cmake_dir "<INSTALL_DIR>/lib/cmake/paraview-${paraview_version}/vtk")
endif ()

set(opencv_platform_dependencies)
if (UNIX)
  list(APPEND opencv_platform_dependencies
    ffmpeg)
endif ()

superbuild_add_project(opencv
  DEPENDS cxx17 boost eigen
  DEPENDS_OPTIONAL paraview gdal tbb flann
    ${opencv_platform_dependencies}
  LICENSE_FILES
    LICENSE

  CMAKE_ARGS
    -DCMAKE_BUILD_TYPE=Release
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_RPATH:STRING=<INSTALL_DIR>/lib
    -DWITH_FFMPEG:BOOL=${ffmpeg_enabled}
    -DWITH_TBB:BOOL=${tbb_enabled}
    -DWITH_VTK:BOOL=${paraview_enabled}
    -DWITH_OPENGL:BOOL=ON
    -DWITH_LIBV4L:BOOL=ON
    -DWITH_GDAL:BOOL=${gdal_enabled}
    -DVTK_DIR:PATH=${vtk_cmake_dir}
    -DGFLAGS:BOOL=OFF
    -DBUILD_TESTING:BOOL=OFF
    -DBUILD_EXAMPLES:BOOL=OFF
    -DBUILD_TESTS:BOOL=OFF
    -DBUILD_PERF_TESTS:BOOL=OFF
    -DBUILD_JAVA:BOOL=OFF
    -DBUILD_opencv_flann:BOOL=${flann_enabled}
    -DBUILD_opencv_java_bindings_generator:BOOL=OFF
    -DBUILD_opencv_js_bindings_generator:BOOL=OFF
    -DBUILD_opencv_python3:BOOL=OFF
    -DBUILD_opencv_python_bindings_generator:BOOL=OFF
    -DBUILD_opencv_python_tests:BOOL=OFF
    -DBUILD_opencv_highgui:BOOL=OFF
)

# windows opencv install architecture doesn't match the one used here
if (WIN32)
  superbuild_project_add_step(opencv-copylibs
  COMMAND   "${CMAKE_COMMAND}"
            -Dinstall_location:PATH=<INSTALL_DIR>
            -P "${CMAKE_CURRENT_LIST_DIR}/scripts/opencv.copylibs.cmake"
  DEPENDEES install
  COMMENT   "Copy .dll files to the bin/ directory"
  WORKING_DIRECTORY <SOURCE_DIR>)
endif ()

superbuild_add_project(g2o
  DEPENDS cxx17 eigen ceres
  LICENSE_FILES
    # Most files are licensed under BSD but:
    #   - csparse_extension is licensed under LGPL3+
    #   - g2o_viewer, g2o_incremental and slam2d_g2o are licensed under GPL3+
    #   - some features of CHOLMOD use GPL license
    doc/license-bsd.txt

  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DG2O_BUILD_EXAMPLES:BOOL=OFF
    -DG2O_BUILD_APPS:BOOL=OFF
    -DG2O_USE_LGPL_LIBS:BOOL=OFF
    -DG2O_USE_CHOLMOD:BOOL=OFF
    -DG2O_USE_OPENGL:BOOL=OFF
    -DG2O_USE_VENDORED_CERES:BOOL=OFF
    -Dg2o_RUNTIME_OUTPUT_DIRECTORY:PATH=bin
    -Dg2o_LIBRARY_OUTPUT_DIRECTORY:PATH=lib
    -DCMAKE_INSTALL_LIBDIR:PATH=lib
    -DCMAKE_INSTALL_RPATH:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
)

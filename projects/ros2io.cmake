set(paraview_binary_dir)
if (TARGET paraview)
  set(paraview_binary_dir "<BINARY_DIR>")
  _ep_replace_location_tags(paraview paraview_binary_dir)
endif ()

superbuild_add_project(ros2io
  DEPENDS paraview fastcdr mcap
  DEPENDS_OPTIONAL zstd lz4 qt5 python3
  LICENSE_FILES
    LICENSE

  CMAKE_ARGS
    -DEXTERNAL_MCAP:BOOL=${mcap_enabled}
    -DUSE_ZSTD:BOOL=${zstd_enabled}
    -DUSE_LZ4:BOOL=${lz4_enabled}
    -DCMAKE_INSTALL_LIBDIR:PATH=lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
)

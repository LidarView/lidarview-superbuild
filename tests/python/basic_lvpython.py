import lidarview.simple as lvsmp
import paraview.simple as smp
from paraview.vtk.util.misc import vtkGetDataRoot

AVAILABLE_INTERPRETERS = ["Velodyne", "Hesai"]

data_dir = vtkGetDataRoot()

interpreters = lvsmp.GetAvailableInterpreterList()
assert all(vendor in interpreters for vendor in AVAILABLE_INTERPRETERS)

velodyneData = lvsmp.OpenPCAP(f"{data_dir}/cartest.pcap", "VLP-16", "Velodyne")
hesaiData = lvsmp.OpenPCAP(f"{data_dir}/hesaitest.pcap", "PandarXT-32", "Hesai")

smp.Show(velodyneData)
smp.Show(hesaiData)


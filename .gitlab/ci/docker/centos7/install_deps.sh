#!/bin/sh

set -e

# Install build requirements.
yum install -y \
    freeglut-devel glew-devel graphviz-devel \
    libxcb-devel libXt-devel xcb-util-wm-devel xcb-util-devel \
    xcb-util-image-devel xcb-util-keysyms-devel xcb-util-renderutil-devel \
    libXcursor-devel mesa-libGL-devel mesa-libEGL-devel \
    libxkbcommon-devel libxkbcommon-x11-devel file mesa-dri-drivers autoconf \
    automake libtool chrpath bison flex libXrandr-devel \
    alsa-lib-devel

# Install LidarView build requirements
yum install -y \
    httpd24.x86_64 ca-certificates libatomic

# Install EPEL
yum install -y \
    epel-release

# Install development tools
yum install -y \
    git-lfs

# Install toolchains.
yum install -y \
    centos-release-scl
yum install -y \
    devtoolset-11-gcc-c++ \
    devtoolset-11 \
    devtoolset-11-gcc \
    devtoolset-11-gfortran \
    rh-git227-git-core

yum install -y \
    devtoolset-11-libatomic-devel

yum clean all

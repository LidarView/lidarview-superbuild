#!/bin/sh

set -e

readonly commonsb_repo="https://gitlab.kitware.com/paraview/common-superbuild.git"
readonly commonsb_commit="b8b221114389300496d05acf44f35fc2b06aba00"

readonly commonsb_root="$HOME/commonsb"
readonly commonsb_src="$commonsb_root/src"
readonly commonsb_build_root="$commonsb_root/build"

export PATH=/root/.bin:/root/.bin/cmake/bin:$PATH
readonly launcher="scl enable devtoolset-11 rh-git227 --"

$launcher git clone "$commonsb_repo" "$commonsb_src"
$launcher git -C "$commonsb_src" checkout "$commonsb_commit"

readonly skipped_qt5_modules="qtspeech;qtconnectivity;qtgamepad;qtlocation;qtsensors;qtserialport;qtwayland;qtwebchannel;qtwebengine;qtwebsockets"

commonsb_build () {
    local prefix="$1"
    shift

    $launcher cmake -GNinja \
        -S "$commonsb_src/standalone-qt" \
        -B "$commonsb_build_root" \
        -DENABLE_qt5:BOOL=ON \
        -Dqt_install_location:PATH=$prefix \
        -DQT5_SKIP_MODULES:STRING="$skipped_qt5_modules" \
        "$@"
    $launcher cmake --build "$commonsb_build_root" --parallel 4
}

commonsb_build /root/qt5

rm -rf "$commonsb_root"

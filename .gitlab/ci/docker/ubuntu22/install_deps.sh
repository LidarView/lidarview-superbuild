#!/bin/sh

set -e

apt update

# Install xvfb for offline graphical testing
apt install -y --no-install-recommends \
    xvfb libxcursor1

# Install extra dependencies for ParaView
apt install -y --no-install-recommends \
    libglvnd-dev bzip2 patch doxygen git git-lfs

# Qt extra depency
DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
    tzdata

# Qt dependencies
apt install -y --no-install-recommends \
    qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools qtmultimedia5-dev \
    qtbase5-private-dev libqt5x11extras5-dev libqt5svg5-dev libqt5help5  \
    libqt5opengl5-dev libqt5xmlpatterns5 qttools5-dev qtxmlpatterns5-dev-tools

# Development tools
apt install -y --no-install-recommends \
    clang-tools clang-tidy clang-format cmake cmake-curses-gui \
    gcc g++ libgfortran5 ninja-build curl build-essential chrpath
apt install -y --no-install-recommends --reinstall \
    ca-certificates

# Pcap dependencies
apt install -y --no-install-recommends \
    flex byacc

apt clean all

include("${CMAKE_CURRENT_LIST_DIR}/configure_common.cmake")

# Use standalone qt installed Centos7 docker image
set(Qt5_DIR "/root/qt5/lib/cmake/Qt5" CACHE STRING "")

set(ENABLE_teaserpp ON CACHE BOOL "")
set(ENABLE_clipper ON CACHE BOOL "")

# LidarView-Superbuild

LidarView-Superbuild is a project to build LidarView or any other LidarView-based application (VeloView, OusterView, ...) and its dependencies.
The project is a small wrapper above Paraview-Superbuild, and provide the common dependencies needed by all viewer and some extra ones.

Developped and maintained by Kitware Europe.

# How to use the superbuild

## Requirements

The superbuild tries to provide all of its own dependencies, but some tooling
is assumed to be available on the host machine.

  - Compiler toolchain
    * GCC 4.9 or newer
    * Xcode 10 or newer
    * MSVC 2019
  - Tools
    * `cmake` 3.20.3 or newer (get it at <https://cmake.org/>)
    * `pkg-config` is used on non-Windows platforms to find dependencies in
      some projects.
    * `ninja` (recommanded) 1.8.2 or newer for building (get it at <https://ninja-build.org/>) 
    * Python (if not built by the superbuild) for building packages
    * If building packages on Linux, `chrpath` is required to make relocatable
      packages
    * `Qt5` (recommanded) 5.15.7 Building Qt5 from source is a lengthy process (see [Install QT](Documentation/install_qt.md)) (**required for windows**)
    * `NSIS` 3.04 or higher for windows packaging (get it at <https://nsis.sourceforge.io/Download>)

Using Ninja is strongly recommended on all platforms to speed up compilation, if you do not wish to use it, make sure to remove the `-GNinja` options from the configuration command.

## Building with the superbuild

**Windows specific guidelines**:
  - Must Use MSVC build environment Command Prompt (installed with MSVC) (located at `Windows Start Menu > Visual Studio 20XX > "VS20XX x64 Native Tools Command Prompt"`)
  - Work Directory, source and build paths must be short and close to the root Drive (e.g at `C:\`), the superbuild can easily reach windows maximum limit.
  - Note that CMake variables, like `Qt5_DIR` path argument,  must use **forward slashes**, MSVC would need otherwise to take `\\Q` as a build option.

**Ubuntu specific guidelines**:
  - Make sure graphics drivers are up-to date, ensuring essential graphics packages are installed. If you do not have a graphics card, `mesa` drivers will need to be installed.
  - If using Ubuntu 18 or higher please install [theses additional packages](Documentation/ubuntu_packages.md)

**General guidelines**:
 - Building from scratch can take anywhere from 20 minutes to 2 hours depending on your hardware. To speed up this process, you can use the `-j` option to use all cores on your machine, but you can specify the number of cores to use with `-j N`.
 - A troubleshooting / FAQ section is available [here](Documentation/faq.md)

#### Clone superbuild repository
```
git clone --recursive https://gitlab.kitware.com/LidarView/lidarview-superbuild.git lvsb
```

#### Create build folder
```
mkdir build && cd build
```

#### Configure step
```
cmake ../lvsb -GNinja -DCMAKE_BUILD_TYPE=Release
```
Build options:
- If Qt5 is installed on you local machine see [Configure with installed Qt5](Documentation/install_qt.md#configure-with-qt).
- If you want to use the [SLAM](https://gitlab.kitware.com/keu-computervision/slam/-/tree/master) or Hesai interpreter you can add the `-DENABLE_slam=True` and `-DENABLE_hesaisdk=True` options. More about LidarView features [here](#project-and-features).
- If you want to use local LidarView source, it is explained [here](#build-with-local-lidarview-source).
- Build variables are explained in more in depth [here](#build-variables).

`cmake-gui` or `ccmake` can help you to configure superbuild.

#### Build step
```
cmake --build . -j
```
The lidarview executable will be located at `./install/bin/LidarView`.

If you modified only LidarView sources, you may want to build incrementally as it is much faster than the full build command: `cmake --build superbuild/lidarview/build -j --target install` or `ninja -C superbuild/lidarview/build -j8 install`

#### Packaging step <a name="packaging-step"></a>

The packages may be built using the `cpack-lidarview` tests via `ctest`.
```
ctest -R cpack
```
This will produce an relocatable archive/installer containing LidarView, that can be shared and installed anywhere.

#### Testing

We use git lfs to manage large data files inside our git repository. For instructions on how to install it, you can check [this page](https://git-lfs.com/).

```
cd <lidarview-superbuild/source-dir>
git lfs pull
```
And then to run the test:
```
cd <lidarview-superbuild/build-dir>
ctest
```

## Project and Features <a name="project-and-features"></a>

The superbuild contains multiple projects which may be used to enable
different features within the resulting LidarView build. Most projects involve
downloading and adding the feature to the resulting package, but there are a
few which are used just to enable features within LidarView itself.

The `paraview` and `lidarview` project must be enabled to build LidarView.

The following packages enable other features within LidarView:

  * `slam`: Enable our [SLAM](https://gitlab.kitware.com/keu-computervision/slam/-/tree/master) plugin. More info about how to SLAM is available at [How to SLAM with LidarView](https://gitlab.kitware.com/keu-computervision/slam/-/blob/master/paraview_wrapping/Plugin/doc/How_to_SLAM_with_LidarView.md).
  * `*sdk`: Enable a SDK that will serve as the interpreter within LidarView (e.g velodyne, robosense...).
  * `pdal`: Enable pdal support, it will enable all non-plugin [pdal readers](https://pdal.io/en/latest/stages/readers.html) within LidarView. Enable `xerces` for `e57` reader.
  * `pcl`: Enable some extra filter based on the [Point Cloud Library](https://pointclouds.org/).
  * `ceres`: Required for filters using non-linear least square optimization (e.g autocalibration).
  * `nanoflann`: Required for filters using optimized kdtree (e.g clustering).
  * `opencv`: Required for handling lidar-camera multisensor systems.

The following optionnal packages enable features within the SLAM:
  * `g2o`: Required for graph related features (loop closure, tag detection and/or GNSS).
  * `gtsam`: Required to handle raw IMU data.
  * `teaserpp`: Required to use automatic loop closure detection (Linux only).

## Build Variables <a name="build-variables"></a>

- `superbuild_download_location` (default `${CMAKE_BINARY_DIR}/downloads`):
The location to store downloaded source artifacts. Usually, it is changed
so that it is preserved across a wipe of the build directory.

- `ENABLE_xxx` (generally, default `OFF`): If selected, the `xxx` project
will be built within the superbuild. See above for descriptions of the
various projects. `ENABLE_xxx` flags are not shown for projects which must be
enabled due to a project depending on it (e.g., `pcl` requires
`boost`, so enabling `pcl` will hide the `ENABLE_boost` option).

- `USE_SYSTEM_xxx` (default `OFF`): If selected, the `xxx` project from the
build environment is used instead of building it within the superbuild.
Not all projects support system copies (the flag is not available if so).

- `CMAKE_BUILD_TYPE` (default `Release`): The build type to use for the
build. Can be `Release`, `RelWithDebInfo`, or (on not-Windows) `Debug`..

The following flags affect ParaView / LidarView directly:

- `lidarview_SOURCE_SELECTION` (default `git`): The source to use for
`LidarView` itself. The `source` selection uses the
`lidarview_SOURCE_DIR` variable to look at a checked out `LidarView` source
directory. The `git` selection has the superbuild clone and builds a
checkout of `LidarView` from git repository controlled by the
`lidarview_GIT_REPOSITORY` and `lidarview_GIT_TAG` variables. By default, the
master branch of the main repository is used. The same system is used for ParaView.

- `CMAKE_BUILD_TYPE_lidarview` (default is the same as the superbuild):
`LidarView` may be built with a different build type (e.g., Release vs.
RelWithDebInfo) as the rest of the superbuild using this variable. In
addition to <SAME> which uses CMAKE_BUILD_TYPE, any valid value for
CMAKE_BUILD_TYPE is also valid. Note: Paraview have the same option
with `CMAKE_BUILD_TYPE_paraview`.

- `LIDARVIEW_EXTRA_CMAKE_ARGUMENTS` or `PARAVIEW_EXTRA_CMAKE_ARGUMENTS` (default `""`):
Extra CMake arguments to pass to LidarView / ParaView's configure step.
This can be used to set CMake variables for the build that are otherwise
not exposed in the superbuild itself.

- `SUPERBUILD_ADDITIONAL_FOLDERS`: A semicolon separated list of folder to look
for additional superbuild architecture, see below for more information.

## Build with local LidarView source <a name="build-with-local-lidarview-source"></a>

As explained in  [build variables](#build-variables), build LidarView with local source would just require to clone LidarView and then set `lidarview_SOURCE_SELECTION` and `lidarview_SOURCE_DIR` during the configure step.
```
git clone --recursive https://gitlab.kitware.com/LidarView/lidarview.git lidarview
cd <path/to/build/folder>
cmake . -Dlidarview_SOURCE_SELECTION=source -Dlidarview_SOURCE_DIR=<path/to/lidarview/sources>
```

## Incremental builds

The superbuild is kind of naïve for changes to project sources within the superbuild. This is due to the superbuild not tracking all source files for each project and instead only "stamp files" to indicate the steps performed.

When changing the source of a subproject, the best solution is to delete the "stamp file" for the build step of that project:

```
rm superbuild/<project>/stamp/<project-build>
```

and rerun the superbuild's build step.

#### Superbuild additional folders

The build option, `SUPERBUILD_ADDITIONAL_FOLDERS` is a semicolor separated list of folders
to look for additional superbuild architecture, which enable ParaView superbuild user to build,
install and package projects (including ParaView plugins) which are not part of the ParaView superbuild.

The main use cases it to be able to create a ParaView package using the ParaView superbuild
with a custom ParaView plugin integrated into the package without requiring manual modification of the generated
archives.

These additional superbuild folder should have the expected folder architecture and contain the expected files:

```
.
├── bundle.cmake
├── cmake
│   └── CTestCustom.cmake
├── package.cmake
├── projects
│   ├── projectA.cmake
│   ├── unix
│   │   └── projectB.cmake
│   └── win32
│       └── projectB.cmake
├── projects.cmake
└── versions.cmake
```

The folder architecture `projects/platforms` is the same as the superbuild architecture and should be followed strictly.

`projects.cmake` is a mandatory file that is included at the configuration phase and is expected to append projects
to the `projects` CMake variable, eg:

```
list(APPEND projects projectA projectB)
```

`versions.cmake` is an optional file of the same type of file as the superbuild own `versions.cmake`,
providing versions for projects using `superbuild_set_revision`, usually for the projects added in `projects.cmake`

`projects/**/*.cmake` files are usual superbuild project files, that uses `superbuild_add_project` to defined how to
configure, build and install superbuild project that are usually added in `projects.cmake`.

`package.cmake` is an optional file that is included at the configuration phase during the preparation of the packaging
and is expected to append ParaView plugins to the `paraview_additional_plugins` CMake variable, eg:

```
list(APPEND paraview_additional_plugins myParaViewPlugin)
```

`cmake/CTestCustom.cmake` is an optional file of the same type as the superbuild own `cmake/CTestCustom.cmake`.
It is included during the test phase, just before the packaging.

`bundle.cmake` is an optional file that is included during the packaging phase, similarly to the superbuild own `project.bundle.cmake` files.
It is expected to use superbuild CMake install macro like `superbuild_unix_install_module` to install supplementary binaries or files, eg:

```
superbuild_unix_install_module("${superbuild_install_location}/lib/mylib.so"
  "lib"
  "lib"
  LOADER_PATHS  "${library_paths}"
  LOCATION      "lib")
```

# License and Copyright

Copyright (c) 2023 Kitware, Inc.

The LidarView-Superbuild is distributed is under the Apache 2.0 license, see NOTICE and LICENSE file.

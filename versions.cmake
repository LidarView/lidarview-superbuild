# Please use https links whenever possible because some people
# cannot clone using ssh (git://) due to a firewalled network.
# This maintains the links for all sources used by this superbuild.
# Simply update this file to change the revision.
# One can use different revision on different platforms.
# e.g.
# if (UNIX)
#   ..
# else (APPLE)
#   ..
# endif()

if (WIN32)
  superbuild_set_revision(pcap
    URL "https://gitlab.kitware.com/LidarView/winpcap/-/archive/1.5.0/winpcap-1.5.0.zip"
    URL_MD5 "1753ef1e420d1846ff6ae5231d1d4f48")
else()
  superbuild_set_revision(pcap
    URL "https://www.tcpdump.org/release/libpcap-1.10.4.tar.xz"
    URL_MD5 "5857c2ddfe25f1bd13308b0e3b8d1e97")
endif()

superbuild_set_revision(npcap
  URL "https://npcap.com/dist/npcap-sdk-1.13.zip"
  URL_MD5 "2067b3975763ddf61d4114d28d9d6c9b")

superbuild_set_revision(tins
  URL     "https://github.com/mfontanini/libtins/archive/refs/tags/v4.5.tar.gz"
  URL_MD5 "a8029f396e4c87bd6d04f48b933d8688")

superbuild_set_revision(ceres
  URL "https://github.com/ceres-solver/ceres-solver/archive/refs/tags/2.2.0.zip"
  URL_MD5 "37bf4a9e668f32fbcfa11999794d0659")

superbuild_set_revision(glog
  URL "https://github.com/google/glog/archive/refs/tags/v0.6.0.zip"
  URL_MD5 "1b246d4d0e8a011d33e0813b256198ef")

superbuild_set_selectable_source(lidarview
  # NOTE: When updating this selection, also update the default version in
  # README.md and the LIDARVIEW_VERSION_DEFAULT variable in CMakeLists.txt.
  SELECT 5.0.1
    GIT_REPOSITORY "https://gitlab.kitware.com/LidarView/lidarview.git"
    GIT_TAG        "5.0.1"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY "https://gitlab.kitware.com/LidarView/lidarview.git"
    GIT_TAG        "master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-lidarview")

superbuild_set_selectable_source(slam
  SELECT 2.1.1
    GIT_REPOSITORY "https://gitlab.kitware.com/keu-computervision/slam.git"
    GIT_TAG        "1dff0c5bb544954a1e2c420c80b703f34af6235a"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY "https://gitlab.kitware.com/keu-computervision/slam.git"
    GIT_TAG        "master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-slam")

superbuild_set_selectable_source(ros2io
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY "https://gitlab.kitware.com/LidarView/plugins/ros2-io.git"
    GIT_TAG        "master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-ros2io")

superbuild_set_selectable_source(pclplugin
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY "https://gitlab.kitware.com/LidarView/plugins/pcl-plugin.git"
    GIT_TAG        "master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-pclplugin")

superbuild_set_revision(fastcdr
  URL "https://github.com/eProsima/Fast-CDR/archive/refs/tags/v2.2.2.zip"
  URL_MD5 "1036163a7c1f11e6c0487868ee6395f2")

superbuild_set_revision(mcap
  URL "https://github.com/foxglove/mcap/archive/refs/tags/releases/cpp/v1.3.0.zip"
  URL_MD5 "d7169760e68a5cce61b77b848aa6a461")

superbuild_set_revision(pcl
  URL "https://github.com/PointCloudLibrary/pcl/archive/refs/tags/pcl-1.14.1.zip"
  URL_MD5 "536fb9437ae95a1c16c45995ff06ed63")

superbuild_set_revision(flann
  URL "https://github.com/flann-lib/flann/archive/refs/tags/1.9.2.zip"
  URL_MD5 "9a1f10c0d890a9595f2f4312436af50f")

superbuild_set_revision(teaserpp
  URL "https://github.com/MIT-SPARK/TEASER-plusplus/archive/refs/tags/v2.0.zip"
  URL_MD5 "49553cbf1d8aef5cbf8e3df958467734")

superbuild_set_revision(clipper
  URL "https://github.com/mit-acl/clipper/archive/refs/heads/main.zip"
  URL_MD5 "42db2ac5ebbb780bd4fab01af53a2b6e")

superbuild_set_revision(scs
  URL "https://github.com/cvxgrp/scs/archive/refs/tags/3.2.7.zip"
  URL_MD5 "8956d90700dbe08aef548b6282ce225d")

superbuild_set_revision(opencv
  URL "https://github.com/opencv/opencv/archive/refs/tags/4.11.0.zip"
  URL_MD5 "d2b425c73a45d7b2d4bcbb7ba17d4300")

superbuild_set_revision(nanoflann
  URL "https://github.com/jlblancoc/nanoflann/archive/refs/tags/v1.5.5.zip"
  URL_MD5 "7484148cdd9f07c83ae2920e7c2119b4")

superbuild_set_revision(yaml
  URL "https://github.com/jbeder/yaml-cpp/archive/refs/tags/yaml-cpp-0.7.0.zip"
  URL_MD5 "1e8ca0d6ccf99f3ed9506c1f6937d0ec")

superbuild_set_revision(g2o
  URL "https://github.com/RainerKuemmerle/g2o/archive/refs/tags/20230806_git.zip"
  URL_MD5 "c95d3614349f13a1d4ff5c18b172548a")

superbuild_set_revision(gtsam
  URL "https://github.com/borglab/gtsam/archive/refs/tags/4.2.zip"
  URL_MD5 "e17cfc58af4cc6f1bd639e273523673d")
